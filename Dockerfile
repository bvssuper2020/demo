FROM bitnami/git
WORKDIR /root
RUN apt update && apt install -y vim docker && mkdir .ssh && chmod 700 .ssh
COPY id_rsa /root/.ssh/id_rsa
COPY id_rsa.pub /root/.ssh/id_rsa.pub
RUN chmod 400 /root/.ssh/id_rsa
CMD ["/bin/bash"]
