# CI DEMO
User setup to work with gitlab project https://gitlab.com/bvssuper2020
Through out this demo lets use some unique code to identify one-self. 

## Pre-requistes
- docker


## Clone the project
`git clone https://gitlab.com/bvssuper2020/demo.git`

## Build the demo project
`cd demo`

`docker build -t <id number> -f Dockerfile .` 
Example: `docker build -t 44234443 -f Dockerfile .`

## Launch a container
`docker run -it <id number> /bin/bash`
Example: `docker run -it 44234443 /bin/bash`

## Create a git project
```
mkdir 44234443
cd 44234443
git init
```

## Add your user and email
`git config --local user.name 44234443`
`git config --local user.email 44234443@example.com`

## Create a Dockerfile

```
cat <<EOF>> Dockerfile
FROM ubuntu 
MAINTAINER abc@gmail.com 

RUN apt-get update 
RUN apt-get install –y nginx 
CMD [“echo”,”Image created”]
EOF
```

Now, run `cat Dockerfile` and verify the contents

## Add and submit to remote
```
git add Dockerfile
git commit -m "Nginx app"
git remote add git_origin git@gitlab.com:bvssuper2020/44234443.git
git push git_origin master
```

## CI configuration
```
cat <<EOF>> .gitlab-ci.yml
before_script:
  - docker info

build_image:
  script:
    - docker build -t 44234443 .
EOF
```
## Add the CI
```
git add .gitlab-ci.yml
git commit -m "Build docker using gitlab ci"
git push git_origin master
```




